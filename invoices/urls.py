from django.contrib import admin
from django.urls import path
from rest_framework import routers
from django.conf.urls import include
from .views import InvoiceViewSet, VATCategoriesViewSet, CostsCategoriesViewSet

router = routers.DefaultRouter()
# router.register('users', UserViewSet)
router.register('invoices', InvoiceViewSet)
router.register('vat-categories', VATCategoriesViewSet)
router.register('costs-categories', CostsCategoriesViewSet)

urlpatterns = [
    path('', include(router.urls)),
]