from django.db import models
from django.contrib.auth.models import User
from .constants import *
from django.conf import settings




class CostCategories(models.Model):
    category = models.CharField(max_length=64, )



class VATCategories(models.Model):
    vat_percentage = models.IntegerField()


class Invoice(models.Model):
    VAT_CHOICES = (
        ('21', '21%'),
        ('9', '9%'),
        ('0', '0%')
    )

    invoice_name = models.CharField(max_length=64)
    invoice_amount = models.DecimalField(max_digits=6, decimal_places=2)
    vat_percentage = models.CharField(max_length=2, choices=VAT_CHOICES)
    invoice_date = models.DateField()
    category = models.ForeignKey(CostCategories, on_delete=models.CASCADE)
    invoice_file = models.FileField(upload_to='invoice_files/', blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)