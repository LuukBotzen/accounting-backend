from django.contrib import admin
from .models import CostCategories, VATCategories, Invoice

admin.site.register(CostCategories)
admin.site.register(VATCategories)
admin.site.register(Invoice)

