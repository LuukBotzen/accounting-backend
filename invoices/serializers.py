from rest_framework import serializers
from .models import Invoice, CostCategories, VATCategories


# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'password')
#         extra_kwargs = {'password': {'write_only': True, 'required': True}}
#
#     def create(self, validated_data):
#         user = User.objects.create_user(**validated_data)
#         token = Token.objects.create(user=user)
#         return user


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ('id', 'invoice_name', 'invoice_amount', 'vat_percentage', 'invoice_date', 'category', 'invoice_file')


class CostCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CostCategories
        fields = ('id', 'category')


class VATCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = VATCategories
        fields = ('id', 'vat_percentage')
