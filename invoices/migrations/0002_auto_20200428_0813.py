# Generated by Django 3.0.5 on 2020-04-28 08:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoices', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='invoice_file',
            field=models.FileField(blank=True, null=True, upload_to='invoice_files/'),
        ),
    ]
