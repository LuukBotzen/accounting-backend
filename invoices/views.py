from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
# from django.contrib.auth.models import User
from .models import Invoice, CostCategories, VATCategories
from .serializers import InvoiceSerializer, CostCategoriesSerializer, VATCategoriesSerializer


# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )


class CostsCategoriesViewSet(viewsets.ModelViewSet):
    queryset = CostCategories.objects.all()
    serializer_class = CostCategoriesSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )


class VATCategoriesViewSet(viewsets.ModelViewSet):
    queryset = VATCategories.objects.all()
    serializer_class = VATCategoriesSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )

