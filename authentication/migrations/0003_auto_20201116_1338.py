# Generated by Django 3.1.3 on 2020-11-16 13:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0002_auto_20201113_0917'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customuser',
            old_name='fav_color',
            new_name='company',
        ),
    ]
